#!/usr/bin/perl

use strict;
use warnings;
use LWP::Simple;
use WWW::Mechanize;

# baixar a página do DDTP
my $url = 'https://ddtp.debian.org/ddtss/index.cgi/pt_BR';
my $m = WWW::Mechanize->new();
$m->get($url);
my $ddtp_pagina_principal = $m->content;
die "Nao consegui obter $url" unless defined $ddtp_pagina_principal;

# extrair as descrições recentemente finalizadas
my $recently_translated = $ddtp_pagina_principal;
# tirar as quebras de linha
$recently_translated =~ s/\n//g;
# pegando a seção que interessa
$recently_translated =~ s!.*<h2>Recently translated .*<ol>(.*)</ol>.*!$1!;
# pegando só os nomes dos pacotes separados por espaço
$recently_translated =~ s!<li><a href="[^"]*">([^<]*)</a>[^<]*</li>!$1!g;

#print "recently:  " . $recently_translated . "\n";
# transformado a cadeia de caracteres separada por espaços em uma array
my @recently_translated = split(' ',$recently_translated);

# para cada descrição finalizada
foreach (@recently_translated) {
  print "- recém-traduzido: " . $_ . "\n";
  my $apt_show = `export LANG=en; apt-cache show $_ 2> /dev/null`;
  # pegar a primeira frase da descrição em inglês
  $apt_show =~ s/(.*\n)*Description.*\n (.*) .*\n(.*\n)*/$2/;
  if ($apt_show eq "") {print "  - sem descrição. pulando...\n"; next}; 
  print "  - apt show: " . $apt_show . "\n";
  # buscar por pacotes com esta frase
  my $candidatos = `export LANG=en; apt-cache search '$apt_show' 2> /dev/null`;
  $candidatos =~ s/ .*//g;
  my @c = split(' ',$candidatos);
  if ($#c > 3) { @c = @c[0,3]};
  foreach (@c) {
  print "  - similares: " . $_ . "\n";
    # enfileirar no DDTP
    unless ($_ eq "") {
      $url = 'https://ddtp.debian.org/ddtss/index.cgi/pt_BR/fetch?package=' . $_;
      #print "    - " . $_ . "\n";
      `firefox -new-tab $url`;
      <STDIN>; # uma paradinha para evitar que o script faça besteira
    }
  }
}
