#!/usr/bin/perl

use strict;
use warnings;
use WWW::Mechanize;
use Tie::File;

# abre um arquivo ordenado por fredm
my @popcon;
tie @popcon, 'Tie::File', 'popcon_fredm' or die "problema ao abrir arquivo\n";

# vai lendo o arquivo à procura uma marca de data. espera-se que
# esta marca de data esteja no ponto do arquivo onde o que tem antes
#já foi trazido para o ddtp, e depois, não
my $j = 0;
until ($popcon[$j] =~ /===.*===/) {
  $j++;
}
# daqui pra frente trás para o ddtp cada pacote do arquivo.
# espera-se que este script seja executado num ambiente com
# firefox e que o firefox já esteja autenticado no ddtss

for (my $i = 100; $i > 0; $i--) {
    print "linha: " . $j . " em popcon_fredm\n";
    $j++; # ir para a linha logo abaixo da marca
    # baixar a página do DDTP
    my $url = 'https://ddtp.debian.org/ddtss/index.cgi/pt_BR';
    my $m = WWW::Mechanize->new();
    $m->get($url);
    my $ddtp_pagina_principal = $m->content;
    die "Nao consegui obter $url" unless defined $ddtp_pagina_principal;
    # extrair a quantidade de pacotes na seção 'pending translation'
    my $pending_translation = $ddtp_pagina_principal;
    $pending_translation =~ s/\n//g; # tirar as quebras de linha
    $pending_translation =~ s!.*<h2>Pending translation \(([0-9]*)\).*!$1!;
    print 'pending translation: ' . $pending_translation . "\n";
    # se já tiver muito pacote pra traduzir, parar por aqui
    last if ($pending_translation >= 100);
    `firefox --new-tab https://ddtp.debian.org/ddtss/index.cgi/pt_BR/fetch?package=$popcon[$j]`;
#    my $url_fetch = "https://ddtp.debian.org/ddtss/index.cgi/pt_BR/fetch?package=$popcon[$j]";
#    print 'ERRO. o mechanize não está autenticando'
#    my $m = WWW::Mechanize->new();
#    $m->get($url_fetch);
#    my $ddtp_pagina_pacote = $m->content;
#    die "Nao consegui obter $url" unless defined $ddtp_pagina_principal;
    
    # manda o pacote que acabou de ir pra tradução pra antes do marcador.
    # para isto, troca as linhas $j-1 e $j.
    @popcon[$j , $j - 1] = @popcon[$j - 1 , $j];
    # a linha $j-1 deve estar com o marcador agora
    untie @popcon;
    tie @popcon, 'Tie::File', 'popcon_fredm';    
    #sleep(2);
    #<STDIN>;
}

untie @popcon;
